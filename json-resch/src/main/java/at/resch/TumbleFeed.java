package at.resch;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class TumbleFeed {

	private ArrayList<Post> posts;
	private String title;
	private String description;
	private String name;
	private String timezone;
	private String cname;
	private ArrayList<String> feeds;
	private int postsStart;
	private int postsTotal;
	private boolean postsType;

	private enum State {
		TumbleLog, Photos, Posts, Root
	}

	public TumbleFeed(String query) {
		HttpGet get = new HttpGet(
				"http://puppygifs.net/api/read/json?callback=foo");
		get.setHeader("User-Agent", "Mozilla/5.0 Me iz no browser");
		CloseableHttpClient client = HttpClients.createDefault();
		State state = State.Root;
		try {
			HttpResponse response = client.execute(get);
			if (response.getStatusLine().getStatusCode() == 200) {
				ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
				byte buffer[] = new byte[1024];
				int read;
				InputStream entity = response.getEntity().getContent();
				while ((read = entity.read(buffer)) > 0) {
					arrayOutputStream.write(buffer, 0, read);
				}
				String json = arrayOutputStream.toString();
				json = json.substring(4, json.length() - 3);
				JsonParser parser = Json.createParser(new ByteArrayInputStream(
						json.getBytes()));
				String name = "";
				Post currentPost = null;
				Photo currentPhoto = null;
				while (parser.hasNext()) {
					Event event = parser.next();
					if (event == Event.KEY_NAME) {
						name = parser.getString();
					} else {
					switch (state) {
					case Root: {
						if (event == Event.START_OBJECT) {
							if (name.equals("tumblelog")) {
								state = State.TumbleLog;
							}
						} else if (event == Event.START_ARRAY) {
							if (name.equals("posts")) {
								state = State.Posts;
							}
						} else if (event == Event.VALUE_NUMBER) {
							if (name.equals("posts-start")) {
								this.postsStart = parser.getInt();
							} else if (name.equals("posts-total")) {
								this.postsTotal = parser.getInt();
							}
						} else if (event == Event.VALUE_STRING) {
							if (name.equals("posts-total")) {
								this.postsTotal = Integer.parseInt(parser
										.getString());
							}
						} else if (event == Event.VALUE_FALSE) {
							this.postsType = false;
						} else if (event == Event.VALUE_TRUE) {
							this.postsType = true;
						}
						break;
					}
					case Posts: {
						if(event == Event.START_OBJECT) {
							currentPost = new Post();
						} else if (event == Event.END_OBJECT) {
							if(posts == null) {
								posts = new ArrayList<Post>();
							}
							if(currentPost != null)
								posts.add(currentPost);
							currentPost = null;
						} else if (event == Event.VALUE_STRING) {
							switch (name) {
							case "id":
								currentPost.setId(parser.getString());
								break;
							case "url":
								currentPost.setUrl(parser.getString());
								break;
							case "type":
								currentPost.setType(parser.getString());
								break;
							case "photo-caption":
								currentPost.setPhotoCaption(parser.getString());
								break;
							case "width":
								currentPost.setWidth(Integer.parseInt(parser.getString()));
								break;
							case "height":
								currentPost.setHeight(Integer.parseInt(parser.getString()));
								break;
							case "photo-url-1280":
								currentPost.setPhotoUrlHighRes(parser.getString());
								break;
							case "photo-url-400":
								currentPost.setPhotoUrlMidRes(parser.getString());
								break;
							case "photo-url-75":
								currentPost.setPhotoUrlLowRes(parser.getString());
								break;
							default:
								break;
							}
						} else if (event == Event.VALUE_NUMBER) {
							if(name.equals("unix-timestamp")) {
								currentPost.setUnixTimestamp(parser.getLong());
							}
						} else if (event == Event.START_ARRAY) {
							state = State.Photos;
						}
						break;
					}
					case Photos: {
						if(event == Event.END_ARRAY) {
							state = State.Posts;
						} else if (event == Event.START_OBJECT) {
							currentPhoto = new Photo();
						} else if (event == Event.END_OBJECT) {
							if(currentPost != null) {
								if(currentPost.getPhotos() == null) {
									currentPost.setPhotos(new ArrayList<Photo>());
								}
								currentPost.getPhotos().add(currentPhoto);
							}
						} else if (event == Event.VALUE_STRING) {
							switch(name) {
							case "offset":
								currentPhoto.setOffset(parser.getString());
								break;
							case "caption":
								currentPhoto.setCaption(parser.getString());
								break;
							case "width":
								currentPhoto.setWidth(Integer.parseInt(parser.getString()));
								break;
							case "height":
								currentPhoto.setHeight(Integer.parseInt(parser.getString()));
								break;
							case "photo-url-1280":
								currentPhoto.setPhotoUrlHighRes(parser.getString());
								break;
							case "photo-url-400":
								currentPhoto.setPhotoUrlMidRes(parser.getString());
								break;
							case "photo-url-75":
								currentPhoto.setPhotoUrlLowRes(parser.getString());
								break;
							default:
								break;
							}
						}
						break;
					}
					case TumbleLog: {
						if (event == Event.VALUE_STRING) {
							switch (name) {
							case "title":
								this.title = parser.getString();
								break;
							case "description":
								this.description = parser.getString();
								break;
							case "name":
								this.name = parser.getString();
								break;
							case "timezone":
								this.timezone = parser.getString();
								break;
							case "cname":
								this.cname = parser.getString();
								break;
							default:
								break;
							}
						} else if (event == Event.END_OBJECT) {
							state = State.Root;
						}
						break;
					}
					}
				}
				}
			} else {
				System.err.println("Could not fetch Data Source");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Post> getPosts() {
		return posts;
	}

	public void setPosts(ArrayList<Post> posts) {
		this.posts = posts;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public ArrayList<String> getFeeds() {
		return feeds;
	}

	public void setFeeds(ArrayList<String> feeds) {
		this.feeds = feeds;
	}

	public int getPostsStart() {
		return postsStart;
	}

	public void setPostsStart(int postsStart) {
		this.postsStart = postsStart;
	}

	public int getPostsTotal() {
		return postsTotal;
	}

	public void setPostsTotal(int postsTotal) {
		this.postsTotal = postsTotal;
	}

	public boolean isPostsType() {
		return postsType;
	}

	public void setPostsType(boolean postsType) {
		this.postsType = postsType;
	}
	
	public String getInfo() {
		return "TumbleFeed [title=" + title
				+ ", description=" + description + ", name=" + name
				+ ", timezone=" + timezone + ", cname=" + cname + ", feeds="
				+ feeds + ", postsStart=" + postsStart + ", postsTotal="
				+ postsTotal + ", postsType=" + postsType + "]";
	}

	@Override
	public String toString() {
		return "TumbleFeed [posts=" + posts + ", title=" + title
				+ ", description=" + description + ", name=" + name
				+ ", timezone=" + timezone + ", cname=" + cname + ", feeds="
				+ feeds + ", postsStart=" + postsStart + ", postsTotal="
				+ postsTotal + ", postsType=" + postsType + "]";
	}

}
