package at.resch;

import java.util.ArrayList;


public class Post {
	
	private String id;
	private String url;
	private String type;
	private long unixTimestamp;
	private String format;
	private String photoCaption;
	private int width;
	private int height;
	private String photoUrlHighRes;
	private String photoUrlMidRes;
	private String photoUrlLowRes;
	
	private ArrayList<Photo> photos;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getUnixTimestamp() {
		return unixTimestamp;
	}

	public void setUnixTimestamp(long unixTimestamp) {
		this.unixTimestamp = unixTimestamp;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getPhotoCaption() {
		return photoCaption;
	}

	public void setPhotoCaption(String photoCaption) {
		this.photoCaption = photoCaption;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getPhotoUrlHighRes() {
		return photoUrlHighRes;
	}

	public void setPhotoUrlHighRes(String photoUrlHighRes) {
		this.photoUrlHighRes = photoUrlHighRes;
	}

	public String getPhotoUrlMidRes() {
		return photoUrlMidRes;
	}

	public void setPhotoUrlMidRes(String photoUrlMidRes) {
		this.photoUrlMidRes = photoUrlMidRes;
	}

	public String getPhotoUrlLowRes() {
		return photoUrlLowRes;
	}

	public void setPhotoUrlLowRes(String photoUrlLowRes) {
		this.photoUrlLowRes = photoUrlLowRes;
	}

	public ArrayList<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(ArrayList<Photo> photos) {
		this.photos = photos;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", url=" + url + ", type=" + type
				+ ", unixTimestamp=" + unixTimestamp + ", format=" + format
				+ ", photoCaption=" + photoCaption + ", width=" + width
				+ ", height=" + height + ", photoUrlHighRes=" + photoUrlHighRes
				+ ", photoUrlMidRes=" + photoUrlMidRes + ", photoUrlLowRes="
				+ photoUrlLowRes + ", photos=" + photos + "]";
	}
	
	public String getInfo() {
		return "Post [id=" + id + ", url=" + url + ", type=" + type
				+ ", unixTimestamp=" + unixTimestamp + ", format=" + format
				+ ", width=" + width
				+ ", height=" + height + ", photoUrlHighRes=" + photoUrlHighRes
				+ ", photoUrlMidRes=" + photoUrlMidRes + ", photoUrlLowRes="
				+ photoUrlLowRes + "]";
	}
	
	
}
