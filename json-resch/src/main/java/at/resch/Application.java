package at.resch;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Import;


@Import(DefaultConfig.class)
public class Application implements CommandLineRunner {

	public static void main(String[] args) {
		new SpringApplicationBuilder(Application.class)
	    .showBanner(false)
	    .logStartupInfo(false)
	    .run(args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		TumbleFeed tumbleFeed = new TumbleFeed("");
		System.out.println(tumbleFeed.getInfo());
		for(Post post : tumbleFeed.getPosts()) {
			System.out.println("\t" + post.getInfo());
			if(post.getPhotos() != null) {
				for(Photo photo : post.getPhotos()) {
					System.out.println("\t\t" + photo.getInfo());
				}
			}
		}
	}

}
