package at.resch;

public class Photo {

	private String offset;
	private String caption;
	private int width;
	private int height;
	private String photoUrlHighRes;
	private String photoUrlMidRes;
	private String photoUrlLowRes;

	public String getOffset() {
		return offset;
	}

	public void setOffset(String offset) {
		this.offset = offset;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getPhotoUrlHighRes() {
		return photoUrlHighRes;
	}

	public void setPhotoUrlHighRes(String photoUrlHighRes) {
		this.photoUrlHighRes = photoUrlHighRes;
	}

	public String getPhotoUrlMidRes() {
		return photoUrlMidRes;
	}

	public void setPhotoUrlMidRes(String photoUrlMidRes) {
		this.photoUrlMidRes = photoUrlMidRes;
	}

	public String getPhotoUrlLowRes() {
		return photoUrlLowRes;
	}

	public void setPhotoUrlLowRes(String photoUrlLowRes) {
		this.photoUrlLowRes = photoUrlLowRes;
	}

	@Override
	public String toString() {
		return "Photo [offset=" + offset + ", caption=" + caption + ", width="
				+ width + ", height=" + height + ", photoUrlHighRes="
				+ photoUrlHighRes + ", photoUrlMidRes=" + photoUrlMidRes
				+ ", photoUrlLowRes=" + photoUrlLowRes + "]";
	}
	
	public String getInfo() {
		return toString();
	}

}
